from .subscribable import Subscribable

class Computable(Subscribable):
    """Computable field."""
    def __init__(self, read=None, write=None, deps=()):
        Subscribable.__init__(self)
        self.__deps = deps
        self.__read = read
        self.__write = write
        for dep in self.__deps: dep.subscribe(self.trigger)
    def __del__(self):
        for dep in self.__deps: dep.unsubscribe(self.trigger)
        Subscribable.__del__(self)
    def trigger(self, val):
        Subscribable.set(self, self.get())
    def get(self):
        Subscribable.get(self)
        return self.__read(*self.__deps)
    def set(self, val):
        self.__write(val, *self.__deps)
        Subscribable.set(self, val)
