class Subscribable(object):
    """Base class for all handled fields."""
    def __init__(self):
        self.__pre = None # Previous value
        self.__cbs = set() # Notified callbacks
    def __call__(self, *args):
        if args: return self.set(args[0])
        else: return self.get()
    def subscribe(self, func):
        self.__cbs.add(func)
        return self
    def unsubscribe(self, func):
        self.__cbs.pop(func)
        return self
    def get(self):
        return None
    def set(self, val):
        if val != self.__pre:
            self.__pre = val
            for func in self.__cbs: func(val)
        return self
