from .subscribable import Subscribable

class Observable(Subscribable):
    """Observable field."""
    def __init__(self, init = None):
        Subscribable.__init__(self)
        self.__def = init
        self.__val = self.__def
        #self.reset()
    def get(self):
#        Subscribable.get(self)
        return self.__val
    def set(self, val):
        self.__old = self.__val
        self.__val = val
        if self.__old != self.__val: Subscribable.set(self, val)
    def reset(self):
        self.set(self.__def)
