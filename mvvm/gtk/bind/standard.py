from .binding import *

class Visible(SingleBinding):
    prop = 'visible'
    type = bool

class Sensitive(SingleBinding):
    prop = 'sensitive'
    type = bool

class Editable(SingleBinding):
    prop = 'editable'
    type = bool

class Text(SingleBinding):
    prop = 'text'
    type = str
    event = 'changed'

class PlaceholderText(SingleBinding):
    prop = 'placeholder-text'
    type = str

class Label(SingleBinding):
    prop = 'label'
    type = str

class Click(SingleBinding):
    event = 'clicked'

class Active(SingleBinding):
    prop = 'active'
    type = bool
