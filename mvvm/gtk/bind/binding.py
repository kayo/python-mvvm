class Binding:
    """Base class for all bindings."""
    def __init__(self, node):
        self.node = node
    def __call__(self):
        self.attach()
    def __del__(self):
        self.detach()
    def attach(): pass
    def detach(): pass

class SingleBinding(Binding):
    """Base class for single bindings."""
    def __init__(self, node):
        Binding.__init__(self, node)
        if self.prop and not self.event:
            self.event = 'notify::' + self.prop
    def __call__(self, field):
        self.field = field
        Binding.__call__(self)

    type = None
    def to(self, val):
        """Value type conversion util.

        If Binding.type member must be correct type for use this feature.
        """
        return self.type(val) if self.type else val
    
    prop = None
    def notify(self, val):
        val = self.to(val)
        if self.node.get_property(self.prop) != val:
            self.node.set_property(self.prop, val)
    
    event = None
    def handle(self, *args):
        if callable(self.field):
            if self.prop: self.field(self.node.get_property(self.prop))
            else: self.field()

    def attach(self):
        try: self.field.subscribe(self.notify)
        except: pass
        # setup value
        if self.prop: self.notify(self.field())
        # attach event
        if self.event: self.handler = self.node.connect(self.event, self.handle)
    def detach(self):
        try: self.field.unsubscribe(self.notify)
        except: pass
        # detach event
        if self.event and self.node.handler_is_connected(self.handler):
            self.node.disconnect(self.handler)
