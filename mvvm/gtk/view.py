from os import path as Path
from gi.repository import Gtk

from . import bind
from .bind import Binding

class View(Gtk.Builder):
    def glade(self, path, name=None):
        path=Path.realpath(path)
        path=path.replace('.py', '')
        if name:
            path=Path.join(Path.dirname(path), name)
        path+='.glade'
        self.add_from_file(path)
        return self
    def __init__(self):
        self._bind_ = set()
        Gtk.Builder.__init__(self)
    def __getitem__(self, name):
        return self.get_object(name)
    def attach(self, model):
        self.connect_signals(model)
    def wrapBindings(self, pool, node):
        for name in dir(bind):
            def cl(name, clas):
                if type(clas) == type(Binding) and issubclass(clas, Binding):
                    def wrap(*args, **kwargs):
                        inst = clas(node)
                        inst(*args, **kwargs)
                        return inst
                    pool[name] = wrap
            cl(name, getattr(bind, name))
    def wrapContext(self, pool, vm):
        for name in dir(vm):
            pool[name] = getattr(vm, name)
    def bind(self, spec, vm):
        while len(self._bind_): self._bind_.pop(0)
        for name in spec:
            node = self[name]
            loc = {}
            self.wrapBindings(loc, node)
            self.wrapContext(loc, vm)
            bindings = spec[name]
            for binding in bindings:
                try:
                    inst = eval(binding, loc)
                except SyntaxError, e:
                    raise e
                except NameError, e:
                    raise e
                else:
                    self._bind_.add(inst)
    def main(self):
        Gtk.main()
