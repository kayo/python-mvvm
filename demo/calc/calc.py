#!/usr/bin/env python

from mvvm.gtk import View

from gi.repository import Gtk

from calc_vm import Model

m = Model()

v = View().glade(__file__)

v['win'].connect('destroy', Gtk.main_quit)
v['win'].show_all()

s = {
    'num': [
        'Editable(editable)',
        'Text(rawinput)',
        'PlaceholderText(result)',
        #'Prop()'
    ],
    'raw': [
        'Active(editable)'
    ]
}

for name in ['clr', 'dot', 'null', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine', 'add', 'sub', 'mul', 'div', 'res']:
    s[name] = ['Click('+name+')']

v.bind(s, m)

Gtk.main()
