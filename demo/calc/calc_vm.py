#--*-- coding: utf-8 --*--

from mvvm.hold import *

class Model(object):
    def __init__(self):
        self.rawinput = Observable('')
        self.editable = Observable(False)
        self.equation = Computable(self.convert, None, [self.rawinput])
        self.result = Observable('')
    def validate(self, value):
        return self.rawinput(value.replace(',', '.').replace('/', '÷').replace('-', '‒').replace('*', '×'))
    def convert(self, source):
        return source().replace(',', '.').replace('÷', '/').replace('‒', '-').replace('×', '*')
    def append(self, chunk):
        self.rawinput(self.rawinput() + str(chunk))

    def null(self): self.append(0)
    def one(self): self.append(1)
    def two(self): self.append(2)
    def three(self): self.append(3)
    def four(self): self.append(4)
    def five(self): self.append(5)
    def six(self): self.append(6)
    def seven(self): self.append(7)
    def eight(self): self.append(8)
    def nine(self): self.append(9)

    def add(self): self.append('+')
    def sub(self): self.append('‒')
    def mul(self): self.append('×')
    def div(self): self.append('÷')

    def dot(self): self.append('.')

    def clr(self):
        self.rawinput('')
        self.result(0)
    def res(self):
        src = self.equation()
        self.clr()
        try:
            res = eval(src)
        except:
            self.result('Err')
        else:
            self.result(res)
